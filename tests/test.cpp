#include <traj_gen/traj_gen.hpp>
#include <../tests/matplotlibcpp.h>

namespace plt = matplotlibcpp;

TrajectoryGenerator trajectory;
CartesianState current_state;
double t=0;
std::vector<double> x_traj;
std::vector<double> y_traj;
std::vector<double> z_traj;
std::vector<double> time_traj;
int main(){

    // Single motion
    auto wp1 = WaypointMotion ({
         LinearMotion(1,0.3,-0.66,0,0,0),
         LinearMotionRelative(0.1,0,0,0,0,0)
    });

    // Motion repeated 10 times
    // auto wp1 = RepeatMotion ({
    //      LinearMotionRelative(1,0,0),
    //      LinearMotionRelative(0.1,0,0)
    // },10);

    // Infinite loop
    // auto wp1 = RepeatMotion ({
    //      LinearMotionRelative(1,0,0),
    //      LinearMotionRelative(0.1,0,0)
    // },-1);

    current_state.pose = Affine(0.0,0,0);
    x_traj.push_back(0);
    y_traj.push_back(0);
    z_traj.push_back(0);
    time_traj.push_back(0);
    bool ok = trajectory.build(current_state,wp1);
    trajectory.printInput();
    if (!ok)
    {
        std::cout << "Trajectory input error" << std::endl;
    }

    std::cout << "Initial state : \n" << current_state.pose.getFrame().matrix() << "\n" << std::endl;

    while(trajectory.update() == Result::Working)
    {
        t+=0.001;
        current_state = trajectory.getNextState();
        x_traj.push_back(current_state.pose.getTranslation()(0));
        y_traj.push_back(current_state.pose.getTranslation()(1));
        z_traj.push_back(current_state.pose.getTranslation()(2));
        time_traj.push_back(t);
        // std::cout << "Next state : \n" << current_state.pose.matrix() << "\n" << std::endl;
    }

  
    plt::suptitle("Time evolution");
    plt::subplot(3, 1, 1);
    plt::plot(time_traj,x_traj);
    plt::subplot(3, 1, 2);
    plt::plot(time_traj,y_traj);
    plt::subplot(3, 1, 3);
    plt::plot(time_traj,z_traj);
    plt::show(true);

    plt::plot3(x_traj,y_traj,z_traj);
    plt::show(true);
    std::cout << "Time " << t << '\n';
    std::cout << "Final state : \n" << current_state.pose.getFrame().matrix() << "\n" << std::endl;
}
