#pragma once
#include <ruckig/ruckig.hpp>
#include <traj_gen/trajectory_limits.hpp>
#include <traj_gen/motion_waypoint.hpp> 
#include <Eigen/Dense>
#include <memory>

using namespace ruckig;
#define dof 2


Eigen::Matrix3d rpyToMatrix(double roll, double pitch, double yaw) {
  Eigen::AngleAxisd rollAngle(roll, Eigen::Vector3d::UnitX());
  Eigen::AngleAxisd pitchAngle(pitch, Eigen::Vector3d::UnitY());
  Eigen::AngleAxisd yawAngle(yaw, Eigen::Vector3d::UnitZ());
  Eigen::Quaternion<double> q = yawAngle * pitchAngle * rollAngle;
  return q.matrix();
}


Eigen::Vector3d diffRot(const Eigen::Matrix3d& R_a_b1, const Eigen::Matrix3d& R_a_b2)
{
    Eigen::Matrix3d R_b1_b2 = R_a_b1.transpose() * R_a_b2;
    Eigen::AngleAxisd aa(Eigen::Map<const Eigen::Matrix3d>(R_b1_b2.data()));
    return R_a_b1 * aa.angle() * aa.axis();
}
    
Eigen::Matrix<double,6,1> diffTransform(const Eigen::Affine3d& t_a_b1, const Eigen::Affine3d& t_a_b2)
{
   Eigen::Matrix<double,6,1> d_t1_t2;
   
   d_t1_t2.head<3>() = t_a_b2.matrix().block<3,1>(0,3) - t_a_b1.matrix().block<3,1>(0,3);
   d_t1_t2.tail<3>() = diffRot(t_a_b1.matrix().topLeftCorner<3,3>(),t_a_b2.matrix().topLeftCorner<3,3>());
   return d_t1_t2;
}

class TrajectoryGenerator {

std::shared_ptr<Ruckig<dof>> otg;
InputParameter<dof> input;
OutputParameter<dof> output;
CartesianState m_current_state;
Eigen::Vector3d translation_axis, rot_axis;
CartesianState target_state;
double angle_axis, translation_length;
std::vector<Waypoint>::iterator waypoint_iterator;
WaypointMotion m_motion;
int n_of_repetition ;
std::array<double,dof> min_velocity, min_acceleration;
Result result = Result::Error;
public:

TrajectoryGenerator(){
   otg.reset(new Ruckig<dof>(0.001));
   m_limits = TrajectoryLimits();
   updateTrajectoryLimits(m_limits.getTrajectoryLimits());
   printInput();
}

TrajectoryLimits m_limits;
Result getTrajectoryResult();

bool build( CartesianState current_state,  WaypointMotion motion,double control_cycle=0.001);

bool build( CartesianState current_state,  LinearMotion motion, double control_cycle=0.001)
{
  return build(current_state,WaypointMotion({motion}),control_cycle);
};

void printInput();

void printOutput();

void setMaxVelocity(double translational_velocity,double rotational_velocity);

void setMaxVelocity(Eigen::Vector2d max_velocity);

void setMaxTranslationVelocity(double max_translation_velocity);
void setMaxTranslationAcceleration(double max_translation_acceleration);
void setMaxTranslationJerk(double max_translation_jerk);
void setMaxRotationVelocity(double max_rotation_velocity);
void setMaxRotationAcceleration(double max_rotation_acceleration);
void setMaxRotationJerk(double max_rotation_jerk);


/**
   * \brief
   * Convert a ruckig OutputParameter to a CartesianState object
   * \param OutputParameter<dof> output
   * \return the ruckig ouput in Cartesian state
   */
CartesianState ruckigOutputToCartesianState(OutputParameter<dof> output);

/**
   * \brief
   * Fill the ruckig input InputParameter with a current state, target state and trajectory limits
   * \param CartesianState current_state
   * \param Waypoint target_state
   * \param TrajectoryLimits limits
   */
void cartesianStateToRuckigInput(CartesianState current_state,Waypoint waypoint, TrajectoryLimitsStruct limits);

public:

/**
   * \brief
   * Get the Cartesian state for the next discrete time step specified by control_cycle
   * \return the next Cartesian state
   */
CartesianState getNextState();

// Result update(KDL::Frame input);

/**
   * \brief
   * update the otg for the next discrete time step specified by control_cycle
   * \return a ruckig result message to get the status of the otg
   */
Result update();

/**
   * \brief
   * update the trajectory limits. Can only be used when updating the trajectory online
   * \param TrajectoryLimits limits
   */
void updateTrajectoryLimits(TrajectoryLimitsStruct limits);
};
