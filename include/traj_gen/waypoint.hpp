#pragma once
#include <traj_gen/affine.hpp>



class CartesianState{
   public:

      CartesianState(){
         pose = Affine();
         velocity = Eigen::Matrix<double,6,1>::Zero();
         acceleration = Eigen::Matrix<double,6,1>::Zero();
         jerk = Eigen::Matrix<double,6,1>::Zero();
      }
      Affine pose;
      Eigen::Matrix<double,6,1> velocity;
      Eigen::Matrix<double,6,1> acceleration;
      Eigen::Matrix<double,6,1> jerk;
      void print () {
         std::cout << "Pose\n" << pose.getFrame().matrix() << "\n" <<
                     "Velocity\n" << velocity <<"\n" <<
                     "Acceleration\n" << acceleration << std::endl;
      }
};

/**
 * The Waypoints struct defines a generic motion, it consist of:
 * - an Affine element
 * - a reference type (Absolute or Relative)
 * - a minimum time
 */
struct Waypoint {
    /**
     * @brief Type of motion. 
     * Absolute : Reference is the origin 
     * Relative : Reference is the previous frame
     */
    enum class ReferenceType {
        Absolute,
        Relative
    };

    CartesianState waypoint_state;
    ReferenceType reference_type;
    double minimum_time;

    /**
     * @brief Default constructorr. Affine is the identity matrix. ReferenceType is Absolute.
     */
    explicit Waypoint(): reference_type(ReferenceType::Absolute) {
        waypoint_state.pose = Affine();
    }

    /**
     * @brief Constructorr. Sets the target frame and the reference type.
     * @param affine The target frame
     * @param ReferenceType The ReferenceType. Default is Absolute
     */
    explicit Waypoint(const Affine& affine, ReferenceType reference_type = ReferenceType::Absolute) {
        this->reference_type = reference_type;
        waypoint_state.pose = affine;
    }
    
    /**
     * @brief Constructorr. Sets only the minimum_time. This is used for the HoldPosition. The affine is set to the identity and the ReferenceType is Relative
     * @param minimum_time The minimum time to perform the motion
     */
    explicit Waypoint(double minimum_time){
        this->minimum_time = minimum_time;
        reference_type = ReferenceType::Relative;
        waypoint_state.pose = Affine();
    }

    /**
     * @brief get the target state
     * @return the target state
     */
    CartesianState getTargetState() {return waypoint_state;};

    /**
     * @brief get the target frame
     * @return the target frame
     */
    Affine getTargetFrame() {return waypoint_state.pose;};

    /**
     * @brief get the ReferenceType
     * @return the ReferenceType
     */
    ReferenceType getReferenceType(){return reference_type;};

    /**
     * @brief get the minimum_time
     * @return the minimum_time
     */
    double getMinimumTime() {return minimum_time;};
};


/**
 * The LinearMotion struct defines a linear motion relatively to the origin frame
 */
struct LinearMotion: public Waypoint {
    /**
     * @brief Constructor. Sets the target pose according to an Affine element.
     * @param target the target pose
     */
    explicit LinearMotion(const Affine& target): Waypoint(target) { }
    
    /**
     * @brief Constructor. Sets the target frame.
     * @param x the amount of translation along the x axis.
     * @param y the amount of translation along the y axis
     * @param z the amount of translation along the z axis
     * @param roll the roll angle. Default is 0.
     * @param pitch the pitch angle. Default is 0.
     * @param yaw the yaw angle. Default is 0.
     */
    explicit LinearMotion(double x, double y ,double z, double roll=0.0, double pitch=0.0, double yaw=0.0): Waypoint(Affine(x,y,z,roll,pitch,yaw)) { }

};

/**
 * The LinearMotionRelative struct defines a linear motion relatively to the last frame.
 */
struct LinearMotionRelative: public Waypoint {
    /**
     * @brief Constructor. Sets the target pose according to an Affine element.
     * @param target the target pose
     */
    explicit LinearMotionRelative(const Affine& target): Waypoint(target,Waypoint::ReferenceType::Relative) { }

    /**
     * @brief Constructor. Sets the target frame.
     * @param x the amount of translation along the x axis.
     * @param y the amount of translation along the y axis
     * @param z the amount of translation along the z axis
     * @param roll the roll angle. Default is 0.
     * @param pitch the pitch angle. Default is 0.
     * @param yaw the yaw angle. Default is 0.
     */
    explicit LinearMotionRelative(double x, double y ,double z, double roll=0.0, double pitch=0.0, double yaw=0.0): Waypoint(Affine(x,y,z,roll,pitch,yaw),Waypoint::ReferenceType::Relative) { }
};

/**
 * The HoldPosition struct defines a holding motion for a given amount of time.
 */
struct HoldPosition : public Waypoint {
    /**
     * @brief Constructor. Sets the amount of holding time.
     * @param target the amount of holding time.
     */
    explicit HoldPosition(double duration): Waypoint(duration) { }
};