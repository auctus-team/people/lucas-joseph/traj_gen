#pragma once
#include <Eigen/Dense>


struct Affine{
    private:
    Eigen::Isometry3d m_frame;
    public:
    
    /**
     * \brief
     * Default constructor sets the frame to the identity matrix
     */
    Affine(){m_frame = Eigen::Isometry3d::Identity();}

    /**
     * \brief
     * Default constructor sets the frame to the identity matrix
     */
    Affine(Eigen::Matrix<double,4,4> frame){setFrame(frame);};

    /**
     * \brief Constructor that sets the 3x1 translation vector and the 3x3 rotation matrix.
     * By default the rotation is set to the 3x3 identity matrix.
     * @param translation the translation vector.
     * @param linear the rotation matrix.
     */
    Affine(Eigen::Vector3d translation, Eigen::Matrix3d linear=Eigen::Matrix3d::Identity())
    {
        m_frame = Eigen::Isometry3d::Identity();
        setTranslation(translation);
        setLinear(linear);
    }

    /**
     * @brief   Constructor that sets the 3x1 translation vector and the rotation matrix using the roll, pitch, yaw convention.
     * By default the rotation is set to the 3x3 identity matrix.
     * @param x the amount of translation along the x axis.
     * @param y the amount of translation along the y axis
     * @param z the amount of translation along the z axis
     * @param roll the roll angle. Default is 0.
     * @param pitch the pitch angle. Default is 0.
     * @param yaw the yaw angle. Default is 0.
     */
    Affine(double x, double y ,double z, double roll = 0.0, double pitch = 0.0, double yaw = 0.0)
    {
        m_frame = Eigen::Isometry3d::Identity();
        setTranslation(Eigen::Vector3d (x,y,z));
        setLinear(rpyToMatrix(roll,pitch,yaw));
    }

    /**
     * @brief Set the Affine element according to the given Isometry3d
     * @param frame the desired Isometry3d
     */
    void setFrame( Eigen::Isometry3d frame) {m_frame = frame;};

    /**
     * @brief Set the Affine element according to the given Isometry3d
     * @param frame the desired Isometry3d
     */
    void setFrame( Eigen::Affine3d frame) {m_frame.matrix() = frame.matrix();};

    /**
     * @brief Set the Affine element according to the given Isometry3d
     * @param frame the desired Isometry3d
     */
    void setFrame( Eigen::Matrix<double,4,4> frame) {m_frame.matrix() = frame;};

    /**
     * @brief Get the Isometry3d frame representing the current Affine element
     * @return the current Isometry3d
     */
    Eigen::Isometry3d getFrame() {return m_frame;};
    
    /**
     * @brief Set the translation part of the Affine element according to the given Vector3d
     * @param frame the desired translation
     */
    void setTranslation( Eigen::Vector3d frame_translation) {m_frame.translation() = frame_translation;};

    /**
     * @brief Get the Vector3d representing the translation of the current Affine element
     * @return the current translation
     */
    Eigen::Vector3d getTranslation() {return m_frame.translation();};
    
    /**
     * @brief Set the rotation part of the ffine element according to the given Matrix3d
     * @param frame the desired rotation
     */
    void setLinear( Eigen::Matrix3d frame_linear) {m_frame.linear() = frame_linear;};

    /**
     * @brief Get the Matrix3d representing the rotation of the current Affine element
     * @return the current rotation
     */
    Eigen::Matrix3d getLinear() {return m_frame.linear();};

    /**
     * @brief Prints the element of the Affine function as a 4x4 matrix
     */
    void print() {std::cout << m_frame.matrix() << std::endl;};
    
    /**
     * @brief Creates a 3x3 rotation matrix given the roll, pitch, yaw convention
     * @param roll the roll angle
     * @param pitch the pitch angle
     * @param yaw the yaw angle
     * @return 3x3 rotation matrix
     */
    Eigen::Matrix3d rpyToMatrix(double roll, double pitch, double yaw) {
        Eigen::AngleAxisd rollAngle(roll, Eigen::Vector3d::UnitX());
        Eigen::AngleAxisd pitchAngle(pitch, Eigen::Vector3d::UnitY());
        Eigen::AngleAxisd yawAngle(yaw, Eigen::Vector3d::UnitZ());
        Eigen::Quaternion<double> q = yawAngle * pitchAngle * rollAngle;
        return q.matrix();
    }

};