
#include <traj_gen/affine.hpp>
#include <traj_gen/waypoint.hpp>

struct WaypointMotion {
    int repeat {0};

    std::vector<Waypoint> waypoints;

    explicit WaypointMotion() {}

    explicit WaypointMotion(const std::vector<Waypoint>& waypoints, int repeat = 0): waypoints(waypoints), repeat(repeat) { }
};

struct RepeatMotion : public WaypointMotion {
    explicit RepeatMotion(const std::vector<Waypoint>& waypoints, int repeat = 0): WaypointMotion(waypoints, repeat) { }
};