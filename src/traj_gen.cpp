#include <traj_gen/traj_gen.hpp>


bool TrajectoryGenerator::build(CartesianState current_state, WaypointMotion motion, double control_cycle)
{
    // Initialise Ruckig with control_cyle

    
    m_motion = motion;
    m_current_state = current_state;

    waypoint_iterator =  m_motion.waypoints.begin();
    n_of_repetition = 1;
    cartesianStateToRuckigInput(m_current_state, *waypoint_iterator, m_limits.getTrajectoryLimits());
    result = Result::Working;
    return otg->validate_input(input);
}

void TrajectoryGenerator::cartesianStateToRuckigInput(CartesianState current_state, Waypoint waypoint, TrajectoryLimitsStruct limits)
{
    output.new_position = {0.0,0.0}; // If new input, then output should be reseted
    target_state.pose = waypoint.getTargetFrame();
    if (waypoint.getReferenceType() == Waypoint::ReferenceType::Relative)
    {
        target_state.pose.setTranslation( m_current_state.pose.getTranslation() + target_state.pose.getTranslation());
        target_state.pose.setLinear( m_current_state.pose.getLinear() *  target_state.pose.getLinear());
        
    }
    std::cout << "current_state \n" ; 
    m_current_state.print();
    std::cout << "target_state \n" ; 
    target_state.print();
    input =InputParameter<dof>();
    // Compute the difference between current_state and desired_state
    auto diff = diffTransform(current_state.pose.getFrame(),target_state.pose.getFrame());
    // Compute the translation and rotation axis
    translation_axis = diff.head<3>();
    rot_axis = diff.tail<3>();

    // Compute the translation length and rotation angle
    angle_axis = rot_axis.norm();
    translation_length = translation_axis.norm();

    // Normalize the axis
    rot_axis.normalize();
    translation_axis.normalize();
    
    // Fill input
    // Fill current state
    // Position start at 0 on their path
    input.current_position[0] = 0.0;
    input.current_position[1] = 0.0;

    Eigen::Vector2d::Map( input.current_velocity.data() ) = Eigen::Vector2d (current_state.velocity.head<3>().transpose()*translation_axis,current_state.velocity.tail<3>().transpose()*translation_axis);
    Eigen::Vector2d::Map( input.current_acceleration.data() ) = Eigen::Vector2d (current_state.acceleration.head<3>().transpose()*translation_axis,current_state.acceleration.tail<3>().transpose()*translation_axis);

    // Fill target state
    // Position finnish when path length is reached
    input.target_position[0] = translation_length;
    input.target_position[1] = angle_axis;
    Eigen::Vector2d::Map( input.target_velocity.data() ) = Eigen::Vector2d (0.0,0.0);
    Eigen::Vector2d::Map( input.target_acceleration.data() ) = Eigen::Vector2d (0.0,0.0);

    // Fill trajectory limits
    updateTrajectoryLimits(limits);

    if (!otg->validate_input(input))
        std::cout << "Bad input \n";

    if (waypoint_iterator->getMinimumTime() > 0.0)
        input.minimum_duration = waypoint_iterator->getMinimumTime();
}

Result TrajectoryGenerator::update(){
    
    if (otg->validate_input(input))
    {
        result = otg->update(input,output);
        
        //copy the new kinematic state into the current state
        input.current_position = output.new_position;
        input.current_velocity = output.new_velocity;
        input.current_acceleration = output.new_acceleration;

        // if (result != Result::Finished)
        //     printInput();
        if (result == Result::Finished)
        {
            // std::cout << "Finished" << std::endl;
            // printInput();   
            if (waypoint_iterator != m_motion.waypoints.end()-1)
            {
                m_current_state.pose = getNextState().pose;
                ++waypoint_iterator;
                cartesianStateToRuckigInput(m_current_state,*waypoint_iterator,m_limits.getTrajectoryLimits());
                result = Result::Working;
            }
            else if (n_of_repetition < m_motion.repeat  || m_motion.repeat == -1 )
            {   
                m_current_state.pose = getNextState().pose;
                n_of_repetition++;
                waypoint_iterator =  m_motion.waypoints.begin();
                cartesianStateToRuckigInput(m_current_state,*waypoint_iterator,m_limits.getTrajectoryLimits());
                result = Result::Working;
            }
            else{
                result =  Result::Finished;
            }
        }
    }
    else
    {
        result = Result::ErrorInvalidInput;
        std::cout << "Result::ErrorInvalidInput" << std::endl;
    }

    return result ;
}

Result TrajectoryGenerator::getTrajectoryResult()
{
    return result;
}

void TrajectoryGenerator::printOutput()
{
    std::cout << "Input \new position\n";
    for (const auto& e : output.new_position) 
        std::cout << e << ' ';
    std::cout << " \new velocity\n";
    for (const auto& e : output.new_velocity) 
        std::cout << e << ' ';
    std::cout << " \nnew acceleration\n";
    for (const auto& e : output.new_acceleration) 
        std::cout << e << ' ';
    std::cout << "\n";
}

void TrajectoryGenerator::printInput()
{
    std::cout << "Input \nCurrent position\n";
    for (const auto& e : input.current_position) 
        std::cout << e << ' ';
    std::cout << "\ntranslation_axis\n" << translation_axis.transpose();
    std::cout << "\nrot_axis\n" << rot_axis.transpose();
    std::cout << "\nangle_axis\n" << angle_axis;
    std::cout << "\ntranslation_length\n" << translation_length;
   
    std::cout << " \nCurrent velocity\n";
    for (const auto& e : input.current_velocity) 
        std::cout << e << ' ';
    std::cout << " \nCurrent acceleration\n";
    for (const auto& e : input.current_acceleration) 
        std::cout << e << ' ';
    std::cout << " \nTarget position\n";
    for (const auto& e : input.target_position) 
        std::cout << e << ' ';
    std::cout << " \nTarget velocity\n";
    for (const auto& e : input.target_velocity) 
        std::cout << e << ' ';
    std::cout << " \nTarget acceleration\n";
    for (const auto& e : input.target_acceleration) 
        std::cout << e << ' ';
    // std::cout << " \nMin velocity\n";
    // // for (const auto& e : input.min_velocity) 
    //     std::cout << input.min_velocity.value()[0] << ' ';
    //     std::cout << input.min_velocity.value()[1] << ' ';
    std::cout << " \nMax velocity\n";
    for (const auto& e : input.max_velocity) 
        std::cout << e << ' ';
    // std::cout << " \nMin acceleration\n";
    // // for (const auto& e : input.min_acceleration) 
    //     std::cout << input.min_acceleration.value()[0] << ' ';
    //     std::cout << input.min_acceleration.value()[1] << ' ';
    std::cout << " \nMax acceleration\n";
    for (const auto& e : input.max_acceleration) 
        std::cout << e << ' ';
    std::cout << " \nMax jerk\n";
    for (const auto& e : input.max_jerk) 
        std::cout << e << ' ';
    std::cout << "\n";
}

CartesianState TrajectoryGenerator::getNextState()
{
    return ruckigOutputToCartesianState(output);
}

CartesianState TrajectoryGenerator::ruckigOutputToCartesianState(OutputParameter<dof> output)
{
    CartesianState cartesian_state;
    // Fill linear part
    cartesian_state.pose.setTranslation( m_current_state.pose.getTranslation() +  output.new_position[0] * translation_axis);
    cartesian_state.velocity.head<3>() = output.new_velocity[0] * translation_axis;
    cartesian_state.acceleration.head<3>() = output.new_acceleration[0] * translation_axis;

    // Fill rotation part
    cartesian_state.pose.setLinear(Eigen::AngleAxisd(output.new_position[1],rot_axis) * m_current_state.pose.getLinear());
    cartesian_state.velocity.tail<3>() = output.new_velocity[1] * rot_axis;
    cartesian_state.acceleration.tail<3>() = output.new_acceleration[1] * rot_axis;

    return cartesian_state;
}

void TrajectoryGenerator::updateTrajectoryLimits(TrajectoryLimitsStruct limits)
{
    // Eigen::Vector2d::Map( min_velocity.data()) = limits.min_velocity;
    // input.min_velocity= min_velocity;
    // Eigen::Vector2d::Map( min_acceleration.data()) = limits.min_acceleration;
    // input.min_acceleration = min_acceleration;
    Eigen::Vector2d::Map( input.max_velocity.data() ) = limits.max_velocity;
    Eigen::Vector2d::Map( input.max_acceleration.data() ) = limits.max_acceleration;
    Eigen::Vector2d::Map( input.max_jerk.data() ) = limits.max_jerk;

    if (!otg->validate_input(input))
        std::cout << "Limits: Bad input \n";
}

void TrajectoryGenerator::setMaxVelocity(double translational_velocity,double rotational_velocity)
{
    m_limits.setMaxVelocity(Eigen::Vector2d(translational_velocity,rotational_velocity));
    updateTrajectoryLimits(m_limits.getTrajectoryLimits());
}

void TrajectoryGenerator::setMaxVelocity(Eigen::Vector2d max_velocity)
{
    m_limits.setMaxVelocity(max_velocity);
    updateTrajectoryLimits(m_limits.getTrajectoryLimits());
}

void TrajectoryGenerator::setMaxTranslationVelocity(double max_translation_velocity)
{
    m_limits.setMaxTranslationVelocity(max_translation_velocity);
    updateTrajectoryLimits(m_limits.getTrajectoryLimits());
}

void TrajectoryGenerator::setMaxTranslationAcceleration(double max_translation_acceleration)
{
    m_limits.setMaxTranslationAcceleration(max_translation_acceleration);
    updateTrajectoryLimits(m_limits.getTrajectoryLimits());
}

void TrajectoryGenerator::setMaxTranslationJerk(double max_translation_jerk)
{
    m_limits.setMaxTranslationJerk(max_translation_jerk);
    updateTrajectoryLimits(m_limits.getTrajectoryLimits());
}

void TrajectoryGenerator::setMaxRotationVelocity(double max_rotation_velocity)
{
    m_limits.setMaxRotationVelocity(max_rotation_velocity);
    updateTrajectoryLimits(m_limits.getTrajectoryLimits());
}

void TrajectoryGenerator::setMaxRotationAcceleration(double max_rotation_acceleration)
{
    m_limits.setMaxRotationAcceleration(max_rotation_acceleration);
    updateTrajectoryLimits(m_limits.getTrajectoryLimits());
}

void TrajectoryGenerator::setMaxRotationJerk(double max_rotation_jerk)
{
    m_limits.setMaxRotationJerk(max_rotation_jerk);
    updateTrajectoryLimits(m_limits.getTrajectoryLimits());
}
